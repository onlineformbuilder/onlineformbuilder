## Best online form building workshop   
###  Easy ecommerce and secure data collection websites    
Are you looking for online forms? We supply whatever your form that you required   

#### Our features   

* Form convertion
* Form building
* Optimization
* Conditional logic
* Server rules
* Validation rules   

### Send emails to one or more people upon submission of your form   
Our [form builder](https://formtitan.com) can also set up conditional logic rules to customize messages to form visitors, administrators, or any interested party. Any of your form needs can be fulfilled by our [online form builder](http://www.formlogix.com)   

Happy online form builder!
